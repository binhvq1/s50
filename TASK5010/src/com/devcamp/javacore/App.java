package com.devcamp.javacore;

public class App {
    public static void main(String[] args) {
        System.out.println("New class");
        App.name("HieuHN", 42);
        App app = new App();
        app.name("HieuHN1");
    }
    public static void name(String name, int age) {
        System.out.println("My name: " + name + " and age: " + age);
    }
    public void name(String name)  {
        System.out.println("My name: " + name );
    }
}
