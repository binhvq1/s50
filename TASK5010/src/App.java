import com.devcamp.javacore.AppDevcamp;

public class App {
    public static void main(String[] args) throws Exception {
        //comment 1 line
        String name = "Devcamp is dev code";
        /**
         * Comment nhiều 
         * line
         */
        System.out.println("Hello, World! " + name.length());
        System.out.println("UPERCASE "+"Hello, World! " + name.toUpperCase());
        System.out.println("LOWER CASE "+"Hello, World! " + name.toLowerCase());
        com.devcamp.javacore.App.name("hieuhn3", 23);
        
        com.devcamp.javacore.App app = new com.devcamp.javacore.App();
        app.name("HieuHN2");
        AppDevcamp app1 = new AppDevcamp();
        AppDevcamp.name1();

        app1.name1("HieuHN6");
        
    }
}
